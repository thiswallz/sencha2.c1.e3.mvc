Ext.define('Ejemplo1MVC.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar',
        'Ext.Video'
    ],
    config: {
        tabBarPosition: 'bottom',

        items: [
            {
                title: 'Lista',
                iconCls: 'action',
                items: [
                    {
                        xtype: 'personas'               
                    }
                ]
            },
            {
                title: 'Agregar',
                iconCls: 'add',
                items: [
                    {
                        docked: 'top',
                        xtype: 'titlebar',
                        title: 'Ingresa Persona'
                    },
                    {
                        xtype: 'ingresarpersona'                    
                    }
                ]
            }
        ]
    }
});
