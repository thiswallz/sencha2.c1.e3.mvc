Ext.define('Ejemplo1MVC.view.IngresarPersona', {
    extend: 'Ext.form.Panel',
    alias: "widget.ingresarpersona",    
    requires: [
        'Ext.TitleBar',
        'Ext.form.*',
        'Ext.field.*'
    ],
    config: {
        width: '100%',
        height: '100%',        
        items: [{
            xtype: 'fieldset',
            title: 'Nueva Persona',
            items: [{
                label : 'Nombre',
                xtype: 'textfield',
                required: true,
                name: 'nombre'                
            },{
                label : 'Edad',
                xtype: 'numberfield',
                required: true,
                name: 'edad'                
            },{
                label : 'Mail',
                xtype: 'emailfield',
                required: true,
                name: 'mail'                
            }]
        },            
        {           
            xtype:'container',
            flex : 1,
            layout : {
                type : 'hbox',
                align: 'center'
            },
            items:[{
                xtype: 'button',
                ui : 'confirm',
                text: 'Boton',
                width: '90%',
                margin: 7,
                handler: function(){
                    var form = this.up('ingresarpersona'),
                    field, name, isEmpty;
                    //validacion basica
                    var fields = form.getFields();
                    for (name in fields) {
                        field = fields[name];
                        isEmpty = (!field.getValue() || field.getValue() == "");
                        if (isEmpty) {
                            Ext.Msg.alert('Error', "Ingrese todos los campos obligatorios");
                            return;
                        }                    
                    }
                    //validacion avanzada
                    var model = Ext.create('Ejemplo1MVC.model.Persona', form.getValues());
                    var errors = model.validate();
                    if(errors.all.length>0){
                        Ext.Msg.alert('Error', "Campo "+ errors.all[0]._field + " " + errors.all[0]._message);
                        return;
                    }
                    form.submit({
                        url: 'http://localhost/sencha/c1.core/services/insertapersona.php',
                        method: 'POST',
                        success: function() {
                            Ext.Msg.alert('Info', "Ingresado Correctamente");
                        },
                        failure: function(proxy, response) {
                            var ob = response;
                            Ext.Msg.alert('Alerta', ob.mensaje)
                        }
                    });

                }           
            }]
        }]
    }
});
