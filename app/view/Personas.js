Ext.define('Ejemplo1MVC.view.Personas', {
    extend: 'Ext.Panel',
    alias: "widget.personas",    
    requires: [
        'Ext.TitleBar', 
        'Ext.dataview.List',
        'Ext.data.proxy.Ajax',
        'Ext.data.Store',
        'Ext.util.DelayedTask'
    ],
    config: {
        width: '100%',
        height: '100%',
        items: [{
            xtype : 'toolbar',
            docked: 'top',
            items: [
            { xtype: "spacer" },
            {
                xtype: 'button',
                iconMask: true,
                iconCls: 'refresh',
                ui: 'confirm',
                action: 'refresh',
                itemId: 'refreshId',
                handler: function() {
                    var st = Ext.StoreMgr.get('mgrPersonaId');
                    st.load();
                }
            }]
        },{
            xtype: 'list',
            itemId: 'listaId',
            itemTpl: '{id}: {nombre}',
            width: '100%',
            height: '100%',
            padding: 30,
            pinHeaders: false,
            centered: true,
            store: 'mgrPersonaId',
            loadingText: "Cargando...",
            masked: true
        }]
    }
});
