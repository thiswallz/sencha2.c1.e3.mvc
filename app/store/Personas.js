Ext.define('Ejemplo1MVC.store.Personas', {
    extend: 'Ext.data.Store',
    requires: [
    	'Ejemplo1MVC.model.Persona'
    ],
	config:{
		storeId : 'mgrPersonaId',
	    model: 'Ejemplo1MVC.model.Persona',
        autoSync: true,
        autoLoad: true,
	    proxy:{
            type:'ajax',
            url: 'http://localhost/sencha/c1.core/services/buscapersonas.php',
            reader:{
                type:'json',
                rootProperty:'data'
            }
        }
	}
});